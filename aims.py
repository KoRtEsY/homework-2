#!/usr/bin/env python
from __future__ import division
import math
import numpy as np

def my_function():
    print "Welcome to AIMS module!" 

def std(numbers):
    x_bar = sum(numbers) / float(len(numbers))
    for i in numbers:
        j= (i-x_bar)**2
        summation=sum(j)
        return np.sqrt(summation/float(len(numbers)))
   
